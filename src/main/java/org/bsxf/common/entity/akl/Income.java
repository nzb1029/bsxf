package org.bsxf.common.entity.akl;

import java.util.Date;

import org.bsxf.common.entity.IdEntity;
import org.bsxf.utils.Description;

public class Income extends IdEntity {
	
    /**  描述   (@author: 宁宗彬) */      
	private static final long serialVersionUID = 1L;
	@Description(description = "地块信息")
	private Block block;
	@Description(description = "时间")
	private Date time;
	@Description(description = "农作物单价")
	private double unitPrice;
	@Description(description = "传统方案产量")
	private double normalProduct;
	@Description(description = "爱科农方案产量")
	private double aKLProduct;
	@Description(description = "传统 收益（单价*传统产量）")
	private double normalIncome;
	@Description(description = "爱科农 收益（单价*爱科农产量）")
	private double aKLIncome;
	
	
	public Block getBlock() {
		return block;
	}
	public void setBlock(Block block) {
		this.block = block;
	}
	public Date getTime() {
		return time;
	}
	public void setTime(Date time) {
		this.time = time;
	}
	public double getUnitPrice() {
		return unitPrice;
	}
	public void setUnitPrice(double unitPrice) {
		this.unitPrice = unitPrice;
	}
	public double getNormalProduct() {
		return normalProduct;
	}
	public void setNormalProduct(double normalProduct) {
		this.normalProduct = normalProduct;
	}
	public double getaKLProduct() {
		return aKLProduct;
	}
	public void setaKLProduct(double aKLProduct) {
		this.aKLProduct = aKLProduct;
	}
	public double getNormalIncome() {
		return normalIncome;
	}
	public void setNormalIncome(double normalIncome) {
		this.normalIncome = normalIncome;
	}
	public double getaKLIncome() {
		return aKLIncome;
	}
	public void setaKLIncome(double aKLIncome) {
		this.aKLIncome = aKLIncome;
	}
}
